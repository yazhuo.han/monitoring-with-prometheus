$

1. Create eks cluter

$ eksctl create cluster

# it use a default aws credential creating cluster in default region with default 2 worker nodes.

$ kubectl get node

2. deploy online shop

$ kubectl apply -f ./config-best-practice.yaml

$ kubectl get pod

3. deploy prometheus monitoring stack using the helm chart

$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

$ helm repo update

$ kubectl create namespace monitoring
# we install Prometheus into our own namespace, this way it will be separated from the microservices.

$ helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring

check prometheus monitoring stack status:
$ kubectl --namespace monitoring get pods -l "release=monitoring"
or
$ kubectl get all -n monitoring

- 2 StatefulSets
    - Prometheus Server
        Prometheus Pod is managed by Operator
    - Alert Manager

- 3 Deployments
    - Prometheus Operator
        Created Prometheus and Alertmanager StatefulSet
    - Grafana
    - Kube State Metrics
        - own Helm chart
        - dependency of this Helm chart

- 3 ReplicaSets
    Created by Deployment

- 1 DaemonSet
    - Node Exporter DaemonSet
    DaemonSet runs on every worker node
        - it connects to the server
        - it translates worker node metrics to Prometheus metrics

- Pods
    from Deployments and StatefulSets

- Services
    each components has own

- ConfigMaps
$ kubectl get configmap -n monitoring
    - configurations for different parts
    - managed by operator
    - how to connect to default metrics

- Secrets
$ kubectl get secrets -n monitoring
    - for Grafana
    - for Prometheus
    - for Operator
    - for Alert Manager
- certificates, username & password

- CRDs
$ kubectl get crd -n monitoring
(Custom Resource Definitions)
It's an extension of K8s API.

----Check out components inside Prometheus, Alertmanager, and Operator-------
$ kubectl get statefulset -n monitoring

$ kubectl describe statefulset <name-of-statefulset> -o yaml > statefulset.yaml

-------Everything above got created by helm and managed by operator, imagine creating and managing all of those manually by self!..that's no go---
$

#### Prometheus UI:
$ kubectl port-forward service/monitoring-kube-prometheus-prometheus -n monitoring 9090:9090 &
"&" is to make the command run in the background

result:
Forwarding from 127.0.0.1:9090 -> 9090
Forwarding from [::1]:9090 -> 9090

copy & paste 127.0.0.1:9090 to our browser.--> Prometheus UI
Status--> Configuration
Look at at the configuration file, in scrape configuration we see a list of jobs.
If I go to Targets, we see endpoint,
In Prometheus term,
Instance = an endpoint you can scrape
Job = Collection of Instances with the same purpose.

#### Grafana
$ kubectl port-forward service/monitoring-grafana 8080:80 -n monitoring

resutlt:
Forwarding from 127.0.0.1:8080 -> 3000
Forwarding from [::1]:8080 -> 3000

Copy & paste 127.0.0.1:8080 to the browser.

Default credentials
username: admin
pwd: prom-operator

$
### Create an anomaly to test the monitoring function by triggering a cpu spike ###

similate one pod kept sending another pod request
$ kubectl run curl-test --image=radia/busyboxplus:curl -i --tty --rm
// we will be inside the container of the busy box.

Check the endpoint of our onlineshop:
$ kubectl get svc
result: frontend-external       LoadBalancer   10.100.161.61    a28244a0d61024a51adf13a673d82747-284716622.us-east-1.elb.amazonaws.com 


$ vim test.sh

for i in $(seq 1 10000)
do  
    curl http://a28244a0d61024a51adf13a673d82747-284716622.us-east-1.elb.amazonaws.com > text.txt
done

$ chmod +x test.sh

$ ./test.sh


## Apply Alert Rules
$ kubectl apply -f alert-rules.yaml
# we don't have to define namespace because it's already defined in the alert-rules.yaml file

$ kubectl get PrometheusRule -n monitoring
We can see all the prometheus rule resources

$ kubectl get pod -n monitoring
$ kubectl logs prometheus-monitoring-kube-prometheus-prometheus-0 -n monitoring -c config-reloader

$ kubectl logs prometheus-monitoring-kube-prometheus-prometheus-0 -n monitoring -c prometheus

$
## Alert Manager
$ kubectl port-forward svc/monitoring-kube-prometheus-alertmanager -n monitoring 9093:9093 &

$ kubectl get secret alertmanager-monitoring-kube-prometheus-alertmanager-generated -n monitoring -o yaml | less

[copy the secret]
[decode the secret]

$ echo <the-secret> | base64 -D | less

$
## Create a email-secret.yaml file and keep it locally, 
DO NOT upload it to the git repo

## Apply Alert Manager
$ kubectl apply -f email-secret.yaml
$ kubectl apply -f alert-manager.yaml

## Test Email Alert
$ kubectl run cpu-test --image=containerstack/cpustress -- --cpu 4 --timeout 60s --metrics-brief

$
## Deploy Redis Exporter using Helm Chart
google it
create redis-values.yaml file

$ helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
$ helm repo update
$ helm install [RELEASE_NAME] prometheus-community/prometheus-redis-exporter -f redis-values.yaml
RELEASE_NAME=redis-exporter

check:
$ helm ls
$ kubectl get pod
$ kubectl get servicemonitor redis-exporter-prometheus-redis-exporter -o yaml | less

if go to Prometheus, under Target, redis is there.
If go to main page, type in "redis" we see redis exporter there.

# Create alert rules for redis
check out: "awesome prometheus alerts"

$ kubectl apply -f redis-alert-rules.yaml

$ kubectl get prometheusrule