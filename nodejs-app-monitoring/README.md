#### Monitor Our Own Application
    - no exporter available for our own application
    - we have to define the metrics

    We need Prometheus Client Libraries (need to match the application language)

##### Steps to Monitor Own Application

1. Expose metrics for our nodejs application using nodejs client library.
    - 1. number of requests
    - 2. duration of requests
    (In production, us DevOps engineer ask developers to expose the metrics in the application; Developers write the code using Prometheus client library)
    in browser localhost:3000/metrics
    we are exposing metrics data

    To build the project:

    docker build -t repo-name/image-name:image-tag .
    docker push repo-name/image-name:image-tag

2. Deploy nodejs app in the cluster

    create k8s-config.yaml

    create secrete file:

    $ kubectl create secret docker-registry my-registry-key --docker-server=https://index.docker.io/v1/ --docker-username=codingtruckingjoe --docker-password=xxx

    Configure secret in Deployment

    $ kubectl apply -f k8s-config.yaml
    $ kubectl get pod
    $ kubectl port-forward svc/nodeapp 3000:3000

3. Configure Prometheus to scrape new target (ServiceMonitor)

    Configure Monitor in Deployment

    Create Dashboard in Grafana

To run the nodejs application:

    npm install 
    node app/server.js

To check the application:
go to the browser:
    localhost:3000

